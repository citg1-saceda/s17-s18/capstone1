package com.zuitt.capstone.controllers;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
// Enables cross origin request via @CrossOrigin.
@CrossOrigin
public class CourseController {

    @Autowired
    CourseService courseService;

    //    Create Course
    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse (@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        courseService.createCourse(stringToken, course);
        return new ResponseEntity<>("Course created successfully.", HttpStatus.CREATED);
    }

    //    Get all courses
    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourse(){
        return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
    }

    //    Delete a Course
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable Long courseid, @RequestHeader(value = "Authorization") String stringToken){
        return courseService.deleteCourse(courseid, stringToken);
    }

    //    Update a course
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateCourse(@PathVariable Long courseid,  @RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        return courseService.updateCourse(courseid, stringToken, course);
    }

    //    Get user's courses
    @RequestMapping(value = "/myCourses", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyCourses(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(courseService.getMyCourses(stringToken), HttpStatus.OK);
    }
}
